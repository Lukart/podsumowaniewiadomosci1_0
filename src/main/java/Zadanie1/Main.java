package Zadanie1;

public class Main {
    public static void main(String[] args) {
        String name = "Artur Lukas";
        System.out.println("Modulo 2 ze Stringa: [" + name + "] wynosi: " + modulo(name));
    }

    public static int modulo(String name) {
        int result = 0;

//        String normalize = Normalizer.normalize(name, Normalizer.Form.NFD);
//        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
//        String tempString =  pattern.matcher(normalize).replaceAll("");

        String tempString = name.replaceAll("ą", "a").
                replaceAll("Ą", "a").
                replaceAll("ć", "c").
                replaceAll("Ć", "c").
                replaceAll("ę", "e").
                replaceAll("Ę", "e").
                replaceAll("ł", "l").
                replaceAll("Ł", "l").
                replaceAll("ń", "n").
                replaceAll("Ń", "n").
                replaceAll("ó", "o").
                replaceAll("Ó", "o").
                replaceAll("ś", "s").
                replaceAll("Ś", "s").
                replaceAll("ż", "z").
                replaceAll("Ż", "z").
                replaceAll("ź", "z").
                replaceAll("Ź", "z");

        char[] temp = tempString.toLowerCase().toCharArray();
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] != 32) {
                result += temp[i] - 96;
            }
        }
        return result % 2;
    }
}