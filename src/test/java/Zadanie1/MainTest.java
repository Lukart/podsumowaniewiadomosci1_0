package Zadanie1;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void test1(){
        String name = "Jan Ul";
        int expected = 0;
        Assert.assertEquals(expected, Main.modulo(name));
    }
    @Test
    public void test2(){
        String name = "aaA";
        int expected = 1;
        Assert.assertEquals(expected, Main.modulo(name));
    }
    @Test
    public void test3(){
        String name = "Ąc";
        int expected = 0;
        Assert.assertEquals(expected, Main.modulo(name));
    }
    @Test
    public void test4(){
        String name = "Ąc b";
        int expected = 0;
        Assert.assertEquals(expected, Main.modulo(name));
    }
    @Test
    public void test5(){
        String name = "Artur Lukas";
        int expected = 0;
        Assert.assertEquals(expected, Main.modulo(name));
    }

}
